# holo_ui_resource_sharing

> HOLO UI components for Resource Sharing

[![NPM](https://img.shields.io/npm/v/holo_ui_resource_sharing.svg)](https://www.npmjs.com/package/holo_ui_resource_sharing) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save holo_ui_resource_sharing
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'holo_ui_resource_sharing'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

 © [ianibo](https://github.com/ianibo)
