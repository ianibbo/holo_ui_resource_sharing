import React, { Component } from 'react'

import HoloAppContainer from 'holoreact'
import ResourceSharingModuleComponent from 'holo_ui_resource_sharing'

export default class App extends Component {
  render () {

    HoloAppContainer.getConfig().modules.push({path: '/rs', component: ResourceSharingModuleComponent});
    HoloAppContainer.getConfig().appName='Resource Sharing';

    return (
      <HoloAppContainer />
    )
  }
}
