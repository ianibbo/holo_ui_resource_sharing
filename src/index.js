import React, { Component } from 'react'
import {inject, Provider} from "mobx-react"
import {iosPeople} from 'react-icons-kit/ionicons/iosPeople'
import {Container, DropdownItem, DropdownMenu, DropdownToggle, NavbarBrand, UncontrolledDropdown} from "reactstrap"
import {Link, Redirect, Route} from "react-router-dom"
import {FormattedMessage} from "react-intl"
import Icon from 'react-icons-kit'
import CrudStore from '@holo/holoreact'

const rsCrudStore = new CrudStore()

class ResourceSharingModuleComponent extends Component {

  static getModuleInfo() {
    return {
      label: 'holo.resourcesharing.modulename',
      icon: iosPeople,
      resources: {
        'patronRequest': {
          'editLinkComponent': null,
          'displayLinkComponent': null
        }
      }
    }
  }

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Provider crudStore={rsCrudStore}>
        <Container fluid={true} className='p-0'>
          <nav className='navbar navbar-expand-lg navbar-light bg-light'>
            <div className='container-fluid'>
              <NavbarBrand><Icon className='mr-1' icon={iosPeople} /><FormattedMessage id='holo.resourcesharing.modulename' /></NavbarBrand>
              <div className='collapse navbar-collapse' id='navbarNavAltMarkup'>

                <ul className='navbar-nav mr-auto'>

                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      <FormattedMessage id='holo.resourcesharing.menu' defaultMessage='Resource Sharing' description='Resource Sharing' />
                    </DropdownToggle>

                    <DropdownMenu right>
                      <DropdownItem tag={Link} to='/rs/patronRequests'><FormattedMessage id='holo.resourceSharing.patronRequests.search'
                        defaultMessage='Search Patron Requests'
                        description='Search Patron Requests' /></DropdownItem>
                      <DropdownItem tag={Link} to='/rs/patronRequests/NEW'>
                        <FormattedMessage id='holo.resourceSharing.patronRequests.create'
                          defaultMessage='Create Patron Request'
                          description='Create Patron Request' /></DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </ul>
              </div>
            </div>
          </nav>
        </Container>
      </Provider>
    )
  }
}

export default inject('holoAppStore')(ResourceSharingModuleComponent);


